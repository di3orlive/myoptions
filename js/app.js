'use strict';

(function() {
    $('.tradeTable, .rates, .newsBody, .pieBox, .areaBox, .historyTable, .positionsTable').perfectScrollbar();


    var app = angular.module('MYOptions', ['highcharts-ng', "dragularModule", 'shoppinpal.mobile-menu']);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////dashBoard///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.controller('ratesCtrl', ['$scope', '$element', 'dragularService', '$http', function ($scope, $element, dragularService, $http) {
        $scope.rateTab = 'Currencies';


        $scope.setTab = function(newTab){
            $scope.rateTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.rateTab === tabNum;
        };


        $http.get('allRates.json').success(function(response) {
            $scope.allRates = response;

            function find(array, value) {
                for (var i = 0; i < array.length; i++) {
                    if (array[i] == value) return true;
                }
                return false;
            }

            var currenciesRate = document.querySelector('#currenciesRate');
            var stocksRate = document.querySelector('#stocksRate');
            var indicesRate = document.querySelector('#indicesRate');
            var commoditiesRate = document.querySelector('#commoditiesRate');
            var pieBox = document.querySelector('#pieDropBox');

            dragularService.cleanEnviroment();
            dragularService([currenciesRate], {
                nameSpace: 'currenciesRate',
                containersModel: [$scope.allRates.Currencies.rates],
                accepts: accepts,
                scope: $scope
            });
            dragularService([stocksRate], {
                nameSpace: 'stocksRate',
                containersModel: [$scope.allRates.Stocks.rates],
                accepts: accepts,
                scope: $scope
            });
            dragularService([indicesRate], {
                nameSpace: 'indicesRate',
                containersModel: [$scope.allRates.Indices.rates],
                accepts: accepts,
                scope: $scope
            });
            dragularService([commoditiesRate], {
                nameSpace: 'commoditiesRate',
                containersModel: [$scope.allRates.commodities.rates],
                accepts: accepts,
                scope: $scope
            });
            dragularService([pieBox], {
                nameSpace: ['currenciesRate', 'stocksRate', 'indicesRate', 'commoditiesRate'],
                containersModel: [$scope.allRates.pieBox.rates],
                accepts: accepts,
                scope: $scope
            });

            if(window.innerWidth <= 1240){
                $scope.addInDropBox = function($event, $index) {
                    if($($event.currentTarget).parent().attr('id') == 'currenciesRate'){
                        $scope.allRates.pieBox.rates.push($scope.allRates.Currencies.rates[$index]);
                        $($event.currentTarget).appendTo('#pieDropBox');
                        $scope.allRates.Currencies.rates.splice($index, 1);
                        $event.currentTarget.remove();
                    }else if($($event.currentTarget).parent().attr('id') == 'stocksRate'){
                        $scope.allRates.pieBox.rates.push($scope.allRates.Stocks.rates[$index]);
                        $($event.currentTarget).appendTo('#pieDropBox');
                        $scope.allRates.Stocks.rates.splice($index, 1);
                        $event.currentTarget.remove();
                    }else if($($event.currentTarget).parent().attr('id') == 'indicesRate'){
                        $scope.allRates.pieBox.rates.push($scope.allRates.Indices.rates[$index]);
                        $($event.currentTarget).appendTo('#pieDropBox');
                        $scope.allRates.Indices.rates.splice($index, 1);
                        $event.currentTarget.remove();
                    }else if($($event.currentTarget).parent().attr('id') == 'commoditiesRate'){
                        $scope.allRates.pieBox.rates.push($scope.allRates.commodities.rates[$index]);
                        $($event.currentTarget).appendTo('#pieDropBox');
                        $scope.allRates.commodities.rates.splice($index, 1);
                        $event.currentTarget.remove();
                    }
                };
            }

            function accepts(el, target, source) {
                var elArr = el.getAttribute('class').split(' ');

                if(target.getAttribute('id') == 'currenciesRate' && find(elArr, 'currencies'))
                    return true;
                else if(target.getAttribute('id') == 'stocksRate' && find(elArr, 'stocks'))
                    return true;
                else if(target.getAttribute('id') == 'indicesRate' && find(elArr, 'indices'))
                    return true;
                else if(target.getAttribute('id') == 'commoditiesRate' && find(elArr, 'commodities'))
                    return true;
                else if(target.getAttribute('id') == 'pieDropBox')
                    return true;
                else
                    return false;
            }
        });
    }]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.controller('highchartCtrl', ['$scope', '$http', function($scope, $http) {
        $http.get('graph.json').success(function(response) {
            $scope.highchartsNG = {
                options: {
                    legend: {
                        enabled: false
                    },
                    chart: {
                        type: 'area',
                        backgroundColor: 'transparent'
                    },
                    tooltip: {
                        useHTML: true,
                        backgroundColor: null,
                        borderWidth: 0,
                        shadow: false,
                        formatter: function () {
                            var a = this.point.lost?this.point.lost:"";
                            var b = this.point.won?this.point.won:"";

                            return '<div style="padding: 2px 5px;border-radius: 5px;background-color:'+this.point.bg+';">' +
                                '<span style="font-size: 15px;font-weight: 700;color: #fff;">'+this.point.userName+'</span><br/>' +
                                '<span style="font-size: 12px;color: #fff;">'+this.point.usedCurrency+'</span><br/>' +
                                '<span style="font-size: 12px;color: #fff;">'+a+''+b+'</span>' +
                                '</div>';
                        },
                        borderColor: null
                    },
                    plotOptions: {
                        series: {
                            fillColor: 'rgba(40, 62, 97, .5)'
                            //,
                            //point: {
                            //    events: {
                            //        click: function (e) {
                            //            hs.htmlExpand(null, {
                            //                pageOrigin: {
                            //                    x: e.pageX || e.clientX,
                            //                    y: e.pageY || e.clientY
                            //                },
                            //                headingText: this.usedCurrency,
                            //                maincontentText: this.userName,
                            //                width: 150
                            //            });
                            //        }
                            //    }
                            //}
                        },
                        area: {
                            pointStart: 0,
                            series: {
                                color: '#1c7bbf',
                                fillColor: 'red'
                            },
                            marker: {
                                enabled: true,
                                radius: 0
                                //symbol: 'url(http://di3.click/sites/MYOptions/img/lang/blueUser.png)'
                            }
                        }
                    }
                },
                title: {
                    text: ''
                },
                xAxis: {
                    endOnTick: false,
                    startOnTick: false,
                    type: 'category',
                    title: {
                        text: ''
                    },
                    labels: {style: {color: 'white'}},
                    tickmarkPlacement: 'on'
                },
                yAxis: {
                    title: {text: ''},
                    opposite: true,
                    gridLineColor: '#526a92',
                    labels: {
                        align: 'left',
                        style: {color: 'white'},
                        formatter: function () {
                            return this.value.toFixed(2);
                        }
                    }
                },
                series: [{
                    name: "",
                    data: response[0].gbpUsd
                }]
            };

            $scope.currency = "GBP/USD";
            $scope.changeGraph = function(){
                if($scope.currency == "USD/EUR"){
                    $scope.currency = "GBP/USD";
                    $scope.highchartsNG.series[0].data = response[0].gbpUsd;
                }else{
                    $scope.currency = "USD/EUR";
                    $scope.highchartsNG.series[0].data = response[0].usdEur;
                }

            };
        });
    }]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.controller('newsCtrl', ['$scope', '$http', function($scope, $http) {
        $http.get('news.json').success(function(response) {
            $scope.allNews = response;
        });
    }]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.controller('tradeCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.tradeTab = 'longTrades';

        $scope.setTab = function(newTab){
            $scope.tradeTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.tradeTab === tabNum;
        };


        $http.get('trades.json').success(function(response) {
            $scope.openTrades = response.openTrades;
            $scope.closeTrades = response.closeTrades;
            $scope.predicate = 'id';
            $scope.reverse = false;
            $scope.order = function(predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };
        });
    }]);
////////////////////////////////////////////////////bankHistory/////////////////////////////////////////////////////////
    app.controller('accCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.accTab = 'positions';

        $scope.setTab = function(newTab){
            $scope.accTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.accTab === tabNum;
        };


        $http.get('history.json').success(function(response) {
            $scope.history = response;
        });

        $http.get('positions.json').success(function(response) {
            $scope.positions = response;
        });

    }]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////directives//////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.directive('pieItem', function() {
        var linked = function(scope, element) {
            scope.removeFromDropBox = function() {
                if(scope.allRates.pieBox.rates[scope.index].type == 'currencies'){
                    scope.allRates.Currencies.rates.push(scope.allRates.pieBox.rates[scope.index]);
                    $(element[0]).appendTo('#currenciesRate');
                    scope.allRates.pieBox.rates.splice(scope.index, 1);
                    element.remove();
                }else if(scope.allRates.pieBox.rates[scope.index].type == 'stocks'){
                    scope.allRates.Stocks.rates.push(scope.allRates.pieBox.rates[scope.index]);
                    $(element[0]).appendTo('#stocksRate');
                    scope.allRates.pieBox.rates.splice(scope.index, 1);
                    element.remove();
                }else if(scope.allRates.pieBox.rates[scope.index].type == 'indices'){
                    scope.allRates.Indices.rates.push(scope.allRates.pieBox.rates[scope.index]);
                    $(element[0]).appendTo('#indicesRate');
                    scope.allRates.pieBox.rates.splice(scope.index, 1);
                    element.remove();
                }else if(scope.allRates.pieBox.rates[scope.index].type == 'commodities'){
                    scope.allRates.commodities.rates.push(scope.allRates.pieBox.rates[scope.index]);
                    $(element[0]).appendTo('#commoditiesRate');
                    scope.allRates.pieBox.rates.splice(scope.index, 1);
                    element.remove();
                }
            };


            var circle = angular.element(element[0].querySelector('.pieCircle'));
            var options = {
                size: 125,
                percent: scope.percent || 0,
                timer: scope.timer || 100,
                max: scope.timer || 100,
                lineColor: scope.lineColor || "red",
                timerColor: scope.timerColor || "green",
                lineWidth: 3,
                rotate: 0
            };

            var canvas = angular.element('<canvas></canvas>');
            var span = angular.element('<span></span>');
            if (typeof(G_vmlCanvasManager) !== 'undefined') {
                G_vmlCanvasManager.initElement(canvas);
            }
            circle.append(span);
            circle.append(canvas);
            var ctx = canvas[0].getContext('2d');
            canvas[0].width = canvas[0].height = options.size;

            var circleMargin = 10;
            var radius = (options.size - options.lineWidth - circleMargin) / 2;
            var to_rad = Math.PI / 180;
            var drawCircle = function(color, lineWidth, percent) {
                ctx.save();
                ctx.translate(options.size / 2, options.size / 2);
                ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI);
                percent = Math.min(Math.max(0, percent || 1), 1);
                ctx.beginPath();
                ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
                ctx.strokeStyle = color;
                ctx.lineCap = 'round';
                ctx.lineWidth = lineWidth;
                ctx.stroke();
                ctx.restore();
            };

            function drawArrow(color, percent) {
                percent = Math.min(Math.max(0, percent || 1), 1);
                if (percent == 1) return;
                ctx.save();
                ctx.fillStyle = color;
                ctx.translate(options.size / 2, options.size / 2);
                ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI);
                ctx.beginPath();
                ctx.rotate(Math.PI * 2 * percent);

                var arrowWidth = 10;
                var arrowHeight = 8;

                ctx.moveTo(radius - (arrowWidth / 2), 0);
                ctx.lineTo(radius + (arrowWidth / 2), 0);
                ctx.lineTo(radius, arrowHeight);
                ctx.lineTo(radius - (arrowWidth / 2), 0);
                ctx.fill();
                ctx.restore();
            }

            function drawTicks(color, seconds) {
                var to_rad = Math.PI / 180;
                ctx.save();
                ctx.translate(options.size / 2, options.size / 2);
                ctx.rotate(-90 * to_rad);
                ctx.lineWidth = 1;
                ctx.strokeStyle = color;

                var angle = 360 / 60;
                var tickSize = 14;
                var tickMargin = 8;

                var stepTime = angle * seconds / options.max * 10;
                for (var i = 0; i <= stepTime; i++) {
                    ctx.save();
                    ctx.rotate((360 - (i * angle)) * to_rad);
                    ctx.beginPath();
                    ctx.moveTo(radius - tickSize - tickMargin, 0);
                    ctx.lineTo(radius - tickMargin, 0);
                    ctx.stroke();
                    ctx.restore();
                }
                ctx.restore();
            }

            function render() {
                ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                --options.timer;
                if (options.timer < 0) {
                    options.timer = options.max
                }
                var seconds = options.timer;
                var size = -(options.timer - ((options.max / options.timer) * seconds));

                options.percent = options.percent > 100 ? 100 : (10 * size / options.max * 10);
                drawCircle(options.lineColor, options.lineWidth, options.percent / 100);
                drawArrow(options.lineColor, options.percent / 100);
                drawTicks(options.timerColor, size);
                span.html("<b>" + seconds + "</b>" + "<br/>" + "seconds");
                setTimeout(render, 1000);
            }
            render();
        };

        return {
            restrict: "E",
            scope: {
                timer: "=",
                lineColor: "@",
                timerColor: "@",
                linePercent: "@",
                expiry: "@",
                payout: "@",
                allRates: "=",
                index: "@",
                assent: "@"
            },
            link: linked,
            template:
            '<div class="pieItem">' +
            '<div class="pieLeft">' +
            '<div class="pieHeader">' +
            'expiry' +
            '<h2>{{expiry}}</h2>' +
            '</div>' +
            '<div class="pieCircle" percent="{{linePercent}}" timer="timer" lineColor="lineColor" timerColor="timerColor"></div>' +
            '<div class="pieFooter">' +
            '{{assent}}' +
            '</div>' +
            '</div>' +
            '<div class="pieRight">' +
            '<div class="pieHeader">' +
            'payout' +
            '<h2>{{payout}}</h2>' +
            '</div>' +
            '<div class="pieBtnBox">' +
            '<button type="button" class="high">high <span>78%</span></button>' +
            '<button type="button" class="low">low <span>58%</span></button>' +
            '<button class="remove low" ng-click="removeFromDropBox()">remove</button>' +
            '</div>' +
            '</div>' +
            '</div>'
        };
    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.directive('toggleSelect', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function() {
                    element.toggleClass('active');
                });
                //var h2 = element.find("h2");
                //element.find("li").on("click", function(e){
                //    var selected = angular.element(e.target).html();
                //    h2.html(selected);
                //});
            }
        };
    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.directive('time', function($interval) {
        return {
            restrict: 'E',
            link: function(scope, element) {
                scope.date = new Date();

                $interval(function() {
                    scope.date = new Date();
                }, 1000);
            },
            template: '<h3>time</h3>' +
            '<h2>{{date | date:"HH:mm:ss"}}</h2>'
        };
    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.directive('preLoader', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element) {
                angular.element(document).ready(function () {
                    $timeout(function () {
                        element.remove();
                        angular.element("body")[0].style.overflow= 'visible';
                    }, 500);
                });
            }
        };
    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    app.directive('alertPopup', function() {
//        var linked = function(scope) {
//
//        };
//
//        return {
//            restrict: "E",
//            scope: {
//                title: "@",
//                message: "@"
//            },
//            link: linked,
//            template: '<div class="title">{{title}}</div>' +
//            '<div class="message">{{message}}</div>' +
//            '<span class="close">x</span>'
//        };
//    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////bankHistory/////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


})();

